<?php declare(strict_types = 1);

namespace Semrush\HomeTest\Network;

final class PhpUrlIdGenerator extends AbstractUrlIdGenerator
{
    protected function generateId(string $url) : string
    {
        return $this->bcHexToDec(substr(sha1($url), 0, 16));
    }

    private function bcHexToDec(string $hex): string {
        if(strlen($hex) === 1) {
            return (string) hexdec($hex);
        } else {
            $remain = substr($hex, 0, -1);
            $last = substr($hex, -1);

            return bcadd(bcmul('16', $this->bcHexToDec($remain)), (string) hexdec($last));
        }
    }
}
