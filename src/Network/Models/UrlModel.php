<?php declare(strict_types = 1);

namespace Semrush\HomeTest\Network\Models;

use Semrush\HomeTest\Network\UrlIdGenerator;

class UrlModel {
    private string $url;
    private string $urlId;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getUrlId(): string
    {
        return $this->urlId;
    }

    /**
     * @param string $url
     * @return static
     */
    public static function createInstance(string $url): self {
        return new UrlModel($url);
    }

    /**
     * @param string $url
     */
    public function __construct(string $url) {
        $url = trim($url);
        if ($url === '') {
            throw new \InvalidArgumentException('URL string must not be empty!');
        }

        $this->url = $url;
        $this->urlId = $this->encryptUrl($this->normalizeUrl($this->getUrl()));
    }

    /**
     * @param string $url
     * @return string
     */
    protected function encryptUrl(string $url): string {
        return $this->convertHexToDec(substr(sha1($url), 0, 16));
    }

    /**
     * @param string $hex
     * @return string
     */
    protected function convertHexToDec(string $hex): string {
        if(strlen($hex) === 1) {
            return (string) hexdec($hex);
        } else {
            $remain = substr($hex, 0, -1);
            $last = substr($hex, -1);

            return bcadd(bcmul('16', $this->convertHexToDec($remain)), (string) hexdec($last));
        }
    }

    /**
     * @param string $url
     * @return string
     */
    protected function normalizeUrl(string $url): string {
        return $this->toUpperCaseAscii($this->cleanUrl($url));
    }

    /**
     * @param string $url
     * @return string
     */
    protected function toUpperCaseAscii(string $url): string {
        return \str_ireplace(UrlIdGenerator::ASCII_ENCODINGS, UrlIdGenerator::ASCII_ENCODINGS, $url);
    }

    /**
     * @param string $url
     * @return string
     */
    protected function cleanUrl(string $url): string {
        return match (true) {
            str_starts_with($url, UrlIdGenerator::PROTOCOL_HTTP),
            str_starts_with($url, UrlIdGenerator::PROTOCOL_HTTPS),
            str_starts_with($url, UrlIdGenerator::PROTOCOL_FTP),
            str_starts_with($url, UrlIdGenerator::PROTOCOL_FTPS)
            => $this->removePort($url),
            default
            => $this->removePort(UrlIdGenerator::PROTOCOL_HTTP . $url),
        };
    }

    /**
     * @param string $url
     * @return string
     */
    protected function removePort(string $url): string {
        if (preg_match('/:\d+/', $url, $matches)) {
            $actualPort = substr($matches[0], 1);
            $queryMarkPosition = strpos($url, '?');
            if ($queryMarkPosition > 0 && $actualPort) {
                $portPositionInUrl = strpos($url, $actualPort);
                if ($queryMarkPosition < $portPositionInUrl) {
                    return $url;
                }
            }

            if(in_array($actualPort, [UrlIdGenerator::PORT_HTTPS, UrlIdGenerator::PORT_HTTP, UrlIdGenerator::PORT_HTTPS, UrlIdGenerator::PORT_HTTPS])) {
                return \preg_replace("#:$actualPort#", '', $url, 1);
            }
        }

        return $url;
    }
}
