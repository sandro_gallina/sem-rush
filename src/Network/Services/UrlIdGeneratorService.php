<?php declare(strict_types = 1);

namespace Semrush\HomeTest\Network\Services;

use Semrush\HomeTest\Network\Models\UrlModel;
use Semrush\HomeTest\Network\UrlIdGenerator;

class UrlIdGeneratorService implements UrlIdGenerator {

    /**
     * @inheritDoc
     */
    public function generate(string $url): string {
        $urlModel = UrlModel::createInstance($url);

        return $urlModel->getUrlId();
    }
}

