<?php declare(strict_types = 1);

namespace Semrush\HomeTest\Network;

abstract class AbstractUrlIdGenerator implements UrlIdGenerator
{
    /**
     * Parses an URL to find the expected default port as well as the actual one being used.
     *
     * @param string $url
     * @return string[]
     */
    private function determinePort(string $url) : array
    {
        $protocol = (0 === \strpos($url, self::PROTOCOL_HTTPS) ? self::PROTOCOL_HTTPS : self::PROTOCOL_HTTP);
        $defaultPort = (self::PROTOCOL_HTTPS === $protocol ? self::PORT_HTTPS : self::PORT_HTTP);
        $actualPort = null;
        if (preg_match('/:\d+/', $url, $matches)) {
            $port = substr($matches[0], 1);
            if(in_array($port, [self::PORT_HTTPS, self::PORT_HTTP, self::PORT_HTTPS . self::PORT_HTTPS])){
                $actualPort = $port;
            }

            $queryMarkPosition = strpos($url, '?');

            if ( $queryMarkPosition > 0 && $actualPort) {
                $portPositionInUrl = strpos($url, $actualPort);
                if ($queryMarkPosition < $portPositionInUrl) {
                    $actualPort = null;
                }
            }
        }

        return [$defaultPort, $actualPort];
    }

    /**
     * Checks if the URL starts with the given protocol.
     */
    private function hasProtocol(string $url, ?string $protocol = null) : bool
    {
        if (!$protocol) {
            return false;
        }

        return str_starts_with($url, $protocol);
    }

    /**
     * Checks if the URL contains another URL as a query parameter.
     */
    private function hasUrlParameter(string $url) : bool
    {
        return substr_count($url, 'http') > 1 || substr_count($url, 'www') > 1;
    }

    private function normalizeUrl(string $url) : string
    {
        switch (true) {
            case $this->hasProtocol($url, self::PROTOCOL_HTTP):
            case $this->hasProtocol($url, self::PROTOCOL_HTTPS):
            case $this->hasProtocol($url, self::PROTOCOL_FTP):
            case $this->hasProtocol($url, self::PROTOCOL_FTPS):
                $url = $this->removePort($url);
                break;

            // This leads to URLs with non-http protocols being prefixed with http
            // but is the expected behavior at this point and therefore can't be changed
            case ($this->hasProtocol($url) === false):
                $url = $this->removePort(self::PROTOCOL_HTTP . $url);
                break;
        }

        // Replace lowercase ASCII encoded characters with uppercase
        $url = \str_ireplace(self::ASCII_ENCODINGS, self::ASCII_ENCODINGS, $url);

        return $url;
    }

    /**
     * Removes the port from the authority of an URL.
     *
     * @param string $url
     * @return string
     */
    private function removePort(string $url) : string
    {
        if ($this->hasUrlParameter($url)) {
           return $url;
        }

        [$defaultPort, $actualPort] = $this->determinePort($url);
        if ($actualPort) {
            $url = \preg_replace("#:$actualPort#", '', $url, 1);
        }

        return $url;
    }

    /**
     * Generates a long integer ID from an URL.
     */
    final public function generate(string $url) : string
    {
        $url = \trim($url);

        if ($url === '') {
            throw new \InvalidArgumentException('URL string must not be empty!');
        }

        return $this->generateId($this->normalizeUrl($url));
    }

    abstract protected function generateId(string $url) : string;
}
