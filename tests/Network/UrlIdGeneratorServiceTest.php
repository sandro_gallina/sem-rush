<?php declare(strict_types = 1);

namespace Semrush\HomeTest\Tests\Network;

use PHPUnit\Framework\TestCase;
use Semrush\HomeTest\Network\Services\UrlIdGeneratorService;
use Semrush\HomeTest\Network\UrlIdGenerator;

final class UrlIdGeneratorServiceTest extends TestCase
{
    /**
     * @var UrlIdGenerator
     */
    private UrlIdGenerator $urlGenerator;

    public function setUp() : void
    {
        $this->urlGenerator = new UrlIdGeneratorService();
    }

    public function dataProviderGenerator() : array
    {
        $dataSet = [];
        $file = fopen(__DIR__ . '/../Resources/url_ids.txt', 'r');
        if (false !== $file) {
            while (($line = fgets($file)) !== false) {
                $dataSet[] = explode("\t|\t", trim($line));
            }
            fclose($file);
        }

        return $dataSet;
    }

    /**
     * @test
     * @dataProvider dataProviderGenerator
     */
    public function generateWithValidUrlItReturnsValidUrlId(string $url, string $expectedId) : void
    {
        $generatedId = $this->urlGenerator->generate($url);
        $this->assertSame(
            $expectedId,
            $generatedId,
            sprintf('Expected URL ID generator to return ID [%s], got [%s] instead.', $expectedId, $generatedId)
        );
    }

    /**
     * @test
     */
    public function generateWithEmptyUrlShouldThrowsException() : void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->urlGenerator->generate('');
    }
}
